DELETE FROM `auth_test`.`user`;

INSERT INTO `auth_test`.`user` (`full_name`, `user_email`, `password`, `role`, `token`) VALUES
('John Doe', 'john@doe.com', '$2a$12$iL7otMKdRIFj3StVF4faAO84PdZ6ovUYZCILKBaWfEMxcMLWIPR7W', 'USER',
'eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJpc3MiOiJzZWN1cmUtYXBpIiwiYXVkIjoic2VjdXJlLWFwcCIsInN1YiI6ImpvaG5AZG9lLmNvbSIsImV4cCI6MTU3MTA1ODc1Miwicm9sZSI6WyJVU0VSIl19.VgaII1JhyC7bNBVxhEi65Xsua3ezEdrdZWDpl0-QN24BenS-3raYNBl_3BtqahxWM5nFI7Oa6qoNJZnd1C3bHg');

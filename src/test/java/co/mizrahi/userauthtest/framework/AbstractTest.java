package co.mizrahi.userauthtest.framework;

import co.mizrahi.userauthtest.MyUserAuthAppTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.ws.rs.core.MediaType;

@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = { MyUserAuthAppTest.class }, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@PropertySource("classpath:application.properties")
@AutoConfigureMockMvc
@EnableWebMvc
@Sql(scripts = "/db/test-data.sql")
public abstract class AbstractTest {

    @Autowired
    protected MockMvc mvc;

    protected static ObjectWriter ow = null;
    protected final static ObjectMapper om = new ObjectMapper();

    public AbstractTest() {
        SimpleModule module = new SimpleModule();
        om.registerModule(module);
        ow = om.writer().withDefaultPrettyPrinter();
    }

    protected MockHttpServletRequestBuilder setRequestBuilder(MockHttpServletRequestBuilder builder, String token) {
        return builder.contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .header("Authorization", "Bearer " + token);
    }

    protected MockHttpServletRequestBuilder setRequestBuilder(MockHttpServletRequestBuilder builder) {
        return builder.contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8");
    }
}

package co.mizrahi.userauthtest;

import co.mizrahi.userauthtest.dto.MyUserDto;
import co.mizrahi.userauthtest.framework.AbstractTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserAuthTestAppTests extends AbstractTest {

    @Test
    public void registrationTest() throws Exception {
        var userDto = MyUserDto.builder()
                .fullName("Jane Doe")
                .userEmail("jane@doe.com")
                .password("password")
                .build();
        var registrationRequest = this.setRequestBuilder(
                post("/registration").content(ow.writeValueAsString(userDto)));
        this.mvc.perform(registrationRequest).andExpect(status().is(201));
    }

    @Test
    public void loginTest() throws Exception {
        var login = MyUserDto.builder()
                .userEmail("john@doe.com")
                .password("password")
                .build();
        var loginRequest = this.setRequestBuilder(
                patch("/login").content(ow.writeValueAsString(login)));
        this.mvc.perform(loginRequest).andExpect(status().is(200));
    }

    @Test
    public void getDataTest() throws Exception {
        var getDataRequest = this.setRequestBuilder(get("/data"),
                "eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJpc3MiOiJzZWN1cmUtYXBpIiwiYXVkIjoic2VjdXJlLWFwcCIsInN1Y" +
                        "iI6ImpvaG5AZG9lLmNvbSIsImV4cCI6MTU3MTA1ODc1Miwicm9sZSI6WyJVU0VSIl19.VgaII1JhyC7bNBVxhEi65Xsua3" +
                        "ezEdrdZWDpl0-QN24BenS-3raYNBl_3BtqahxWM5nFI7Oa6qoNJZnd1C3bHg");
        var getDataResponse = this.mvc.perform(getDataRequest).andExpect(status().is(200));
        assertEquals("John Doe", getDataResponse.andReturn().getResponse().getContentAsString());
    }
}

CREATE TABLE user (
    id              BIGINT AUTO_INCREMENT PRIMARY KEY,
    full_name        VARCHAR(60) NOT NULL,
    user_email        VARCHAR(60) NOT NULL UNIQUE,
    password        VARCHAR(60) NOT NULL,
    role       ENUM('USER') DEFAULT 'USER',
    token        VARCHAR(1000)
) ENGINE = InnoDB
  COMMENT 'User table';
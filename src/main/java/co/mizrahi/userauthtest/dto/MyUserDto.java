package co.mizrahi.userauthtest.dto;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter @Builder
public class MyUserDto {

    @Size(max = 60)
    String fullName;

    @Email
    @Size(max = 60)
    String userEmail;

    @NotNull
    @Size(max = 60)
    String password;
}

package co.mizrahi.userauthtest.service;

import co.mizrahi.userauthtest.dao.MyUser;
import co.mizrahi.userauthtest.dao.Roles;
import co.mizrahi.userauthtest.dto.MyUserDto;
import co.mizrahi.userauthtest.repo.MyUserRepo;
import co.mizrahi.userauthtest.security.MyPasswordEncoder;
import co.mizrahi.userauthtest.security.SecurityConstants;
import co.mizrahi.userauthtest.util.ApiUtil;
import co.mizrahi.userauthtest.util.MyException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class MyUserService implements IMyUserService {

    private final ApiUtil util;
    private final MyUserRepo userRepo;
    private final MyPasswordEncoder passwordEncoder;

    @Autowired
    public MyUserService(ApiUtil util, MyUserRepo userRepo, MyPasswordEncoder passwordEncoder) {
        this.util = checkNotNull(util);
        this.userRepo = checkNotNull(userRepo);
        this.passwordEncoder = checkNotNull(passwordEncoder);
    }

    @Override
    public void registration(MyUserDto userDto) {
        this.userRepo.save(MyUser.builder()
                .fullName(userDto.getFullName())
                .userEmail(userDto.getUserEmail())
                .password(this.passwordEncoder.encode(userDto.getPassword()))
                .role(Roles.USER)
                .build());
    }

    @Override
    @Transactional
    public String login(MyUserDto userDto) {
        MyUser user = this.userRepo.findByUserEmail(userDto.getUserEmail()).orElseThrow(()->
                new MyException("User not found", HttpStatus.NOT_FOUND));
        if (!passwordEncoder.matches(userDto.getPassword(), user.getPassword()))
            throw new MyException("Password incorrect", HttpStatus.BAD_REQUEST);
        var signingKey = SecurityConstants.JWT_SECRET.getBytes();
        List<Roles> roles = new ArrayList<>();
        roles.add(user.getRole());
        String token = Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(signingKey), SignatureAlgorithm.HS512)
                .setHeaderParam("type", SecurityConstants.TOKEN_TYPE)
                .setIssuer(SecurityConstants.TOKEN_ISSUER)
                .setAudience(SecurityConstants.TOKEN_AUDIENCE)
                .setSubject(user.getUserEmail())
                .setExpiration(new Date(System.currentTimeMillis() + 864000000))
                .claim("role", roles)
                .compact();
        user.setToken(token);
        return token;
    }

    @Override
    public String getUserData() {
        MyUser user = this.util.getUser();
        return user.getFullName();
    }

    @Override
    public Optional findByToken(String token) {
        Optional<MyUser> userOptional = this.userRepo.findByToken(token);
        if(userOptional.isEmpty()) return  Optional.empty();
        MyUser quoteUser = userOptional.get();
        User user = new User(quoteUser.getUserEmail(), quoteUser.getPassword(),
                true, true, true, true,
                AuthorityUtils.createAuthorityList(String.valueOf(List.of(Roles.USER))));
        return Optional.of(user);
    }
}

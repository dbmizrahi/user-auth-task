package co.mizrahi.userauthtest.service;

import co.mizrahi.userauthtest.dto.MyUserDto;

import java.util.Optional;

public interface IMyUserService {
    void registration(MyUserDto user);
    String login(MyUserDto user);
    String getUserData();

    Optional findByToken(String token);
}

package co.mizrahi.userauthtest.repo;

import co.mizrahi.userauthtest.dao.MyUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MyUserRepo extends JpaRepository<MyUser, Long> {
    Optional<MyUser> findByToken(String token);
    Optional<MyUser> findByUserEmail(String username);
}
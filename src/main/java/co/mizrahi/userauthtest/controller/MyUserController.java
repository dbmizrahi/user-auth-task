package co.mizrahi.userauthtest.controller;

import co.mizrahi.userauthtest.dto.MyUserDto;
import co.mizrahi.userauthtest.service.IMyUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import static com.google.common.base.Preconditions.checkNotNull;

@RestController
public class MyUserController {

    private final IMyUserService userService;

    @Autowired
    public MyUserController(IMyUserService userService) {
        this.userService = checkNotNull(userService);
    }

    @PostMapping(value = "/registration")
    @ResponseStatus(HttpStatus.CREATED)
    public void registration(@RequestBody MyUserDto userDto) {
        this.userService.registration(userDto);
    }

    @PatchMapping(value = "/login")
    public String login(@RequestBody MyUserDto userDto) {
        return this.userService.login(userDto);
    }

    @GetMapping(value = "/data")
    @PreAuthorize("hasRole('USER')")
    public String getUserData() {
        return this.userService.getUserData();
    }
}

package co.mizrahi.userauthtest.dao;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "user")
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter @Builder
public class MyUser {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @Column(name = "full_name")
    String fullName;

    @Column(name = "user_email", unique = true)
    String userEmail;

    String password;

    String token;

    @Enumerated(EnumType.STRING)
    Roles role;
}

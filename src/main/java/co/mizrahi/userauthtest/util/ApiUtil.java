package co.mizrahi.userauthtest.util;

import co.mizrahi.userauthtest.dao.MyUser;
import co.mizrahi.userauthtest.repo.MyUserRepo;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import static java.util.Objects.isNull;

@Service
@Log
public class ApiUtil {

    private final MyUserRepo userRepo;

    @Autowired
    public ApiUtil(MyUserRepo repo){
        this.userRepo = repo;
    }

    public MyUser getUser() {
        String userEmail = this.getUserEmail();
        return this.userRepo.findByUserEmail(userEmail)
                .orElseThrow(()-> new MyException("User is not identified", HttpStatus.UNAUTHORIZED));
    }

    //auth - Authorization header containing access token
    private String getUserEmail() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (isNull(auth)) throw new MyException("User is not identified (TOKEN is missing or wrong!)", HttpStatus.UNAUTHORIZED);

        return auth.getName();
    }

    public static boolean logFailureMessage(boolean condition, String failureMessage){
        if (!condition) log.warning(failureMessage);
        return condition;
    }
}

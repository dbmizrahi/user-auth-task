package co.mizrahi.userauthtest.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.HashMap;

@Data
@NoArgsConstructor
public class MyExceptionInfo {
    LocalDateTime timestamp;
    int status;
    String error;
    String path;
    @JsonIgnore
    MyException e;

    public MyExceptionInfo(MyException e, HttpStatus status, String path){
        this.e = e;
        this.timestamp = LocalDateTime.now();
        this.path = path;
        this.status = status.value();
        this.error = status.getReasonPhrase();
    }

    MyExceptionInfo(MyException e, String path){
        this.e = e;
        this.timestamp = LocalDateTime.now();
        this.path = path;
        this.status = e.getHttpStatus().value();
        this.error = e.getHttpStatus().getReasonPhrase();
    }

    @SuppressWarnings("unchecked")
    public HashMap<String, Object> buildMap(){
        return new ObjectMapper().convertValue(this, HashMap.class);
    }
}

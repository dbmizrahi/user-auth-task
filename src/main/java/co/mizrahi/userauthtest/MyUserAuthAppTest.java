package co.mizrahi.userauthtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyUserAuthAppTest {

    public static void main(String[] args) {
        SpringApplication.run(MyUserAuthAppTest.class, args);
    }

}

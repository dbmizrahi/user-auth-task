# Mycheck Java exam

## Before the first run create schema "auth_test" and do:

```
gradle fC fM
```

## For tests do:

```
gradle test
```

## Run this code in your IDE or do:

```
gradle bR
```